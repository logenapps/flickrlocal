//
//  CollectionVC.m
//  FlickrLocal
//
//  Created by logen on 8/22/15.
//  Copyright (c) 2015 logenw. All rights reserved.
//

#import "CollectionVC.h"

#import "AFNetworking.h"
#import "CollectionHeaderView.h"
#import "Photo.h"
#import "PhotoCell.h"
#import "PhotoVC.h"
#import "UIColor+AppColors.h"
#import "UIImageView+AFNetworking.h"
#import "UIImage+Color.h"

static const CGFloat cellPadding = 3.0f;
static const CGFloat alertHeaderHeight = 30.0f;

@implementation CollectionVC

- (void)viewDidLoad {
    [super viewDidLoad];

    // Basic setup
    self.navigationItem.title = @"Flickr Local";
    _activeSearch = NO;
    _photoIDSet = [NSMutableOrderedSet orderedSetWithCapacity:100];
    _photosDict = [NSMutableDictionary dictionaryWithCapacity:100];
    
    // The placeholder is a simple gray image that is scaled by the collection view
    _placeholderImage = [UIImage imageWithColor:[UIColor placeholderGray]];
    
    // Request authorization to use user's location
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined) {
        NSLog(@"Requesting authorization from the user.");
        [_locationManager requestWhenInUseAuthorization];
    }
    
    // Set up the initial size for the collection view cells
    _isIpad = (self.traitCollection.userInterfaceIdiom == UIUserInterfaceIdiomPad) ? YES : NO;
    [self updateViewForCellSize:self.view.frame.size];
    
    // By default, the alert size is zero (not shown)
    _alertHeaderSize = CGSizeZero;
    
    // Create the UIRefreshControl. Don't add to view until appropriate network/auth confirmed.
    _refreshControl = [[UIRefreshControl alloc] init];
    _refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Fetching more results..."];
    _refreshControl.tintColor = [UIColor systemLightBlue];
    [_refreshControl addTarget:self action:@selector(getPhotos) forControlEvents:UIControlEventValueChanged];
    
    // Monitor if the user has an active internet connection
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        [self networkStatusChange:status];
    }];
}

- (BOOL)canStartSearch {
    @synchronized(self) {
        if (_activeSearch) {
            return NO;
        } else {
            _activeSearch = YES;
            [_refreshControl beginRefreshing];
            return YES;
        }
    }
}

- (void)searchComplete {
    @synchronized(self) {
        _activeSearch = NO;
        [_refreshControl endRefreshing];
    }
}

- (void)getPhotos {
    // Don't allow another request to start while one is in progress.
    if (![self canStartSearch]) {
        return;
    }
    
    // Request data based on _apiSearch, which already has location
    [_apiSearch searchNearby:^(NSDictionary *newPhotos) {
        if (newPhotos.count > 0) {
            // Determine the new photos to add by key
            NSMutableOrderedSet *newPhotoIDSet = [NSMutableOrderedSet orderedSetWithArray:[newPhotos allKeys]];
            [newPhotoIDSet minusOrderedSet:_photoIDSet];
            
            // For each key in the subset, add the associated Photo object to the photo dictionary
            for (NSString *key in newPhotoIDSet) {
                Photo *photo = [newPhotos objectForKey:key];
                [_photosDict setObject:photo forKey:key];
            }
            
            // Put new photos at the top so the user sees them more easily.
            NSUInteger currentCount = (unsigned long)_photoIDSet.count;
            [newPhotoIDSet unionOrderedSet:_photoIDSet];
            
            // Update the datasource on the main thread
            dispatch_async(dispatch_get_main_queue(), ^{
                _photoIDSet = newPhotoIDSet;
                [self.collectionView reloadData];
                [self searchComplete];
                NSLog(@"Added %lu photos, total of: %lu", (unsigned long)currentCount, (unsigned long)_photoIDSet.count);
            });
        } else {
            [self searchComplete];
        }
    }];
}

#pragma mark UICollectionView
- (PhotoCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    // Request the thumbnail while using placeholder.
    PhotoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PhotoCell" forIndexPath:indexPath];
    cell.imageReceived = NO;
    Photo *photo = _photosDict[_photoIDSet[indexPath.item]];
    NSURL *photoURL = _isIpad ? [photo urlForSize:FlickrPhotoSizeSmall] : [photo urlForSize:FlickrPhotoSizeSmall];
    NSURLRequest *request = [NSURLRequest requestWithURL:photoURL];
    __weak PhotoCell *weakCell = cell;
    [cell.imageView setImageWithURLRequest:request
                          placeholderImage:_placeholderImage
                                   success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                       // Use the new image, and mark the property as such
                                       weakCell.imageView.image = image;
                                       weakCell.imageReceived = YES;
                                       [weakCell setNeedsLayout];
                                   } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                                       NSLog(@"%@", error.localizedDescription);
                                   }];
    
    return cell;
}

- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _photoIDSet.count;
}

- (NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (void)updateViewForCellSize:(CGSize)size {
    // Calculate the new cell dimensions based on device type and orientation
    // sideLength <= maxSideLength as UICollectionView will stack left to right, then down
    int cellsPerSmallSide = _isIpad ? 4 : 3;
    CGFloat maxSideLength = fmin(size.width, size.height) / cellsPerSmallSide;
    CGFloat maxCellsForWidth = ceilf(size.width / maxSideLength);
    CGFloat interitemPadding = (maxCellsForWidth - 1) * cellPadding;
    CGFloat sideLength = floorf((size.width - interitemPadding) / maxCellsForWidth);
    _cellSize = CGSizeMake(sideLength, sideLength);
    
    // Attempt to keep the user looking at same items after rotation
    NSArray *sortedVisibleItems = [[self.collectionView indexPathsForVisibleItems] sortedArrayUsingSelector:@selector(compare:)];
    NSIndexPath *currentIndexPath;
    if (sortedVisibleItems.count > 0){
        currentIndexPath = sortedVisibleItems[sortedVisibleItems.count / 2];
    }

    [[self.collectionView collectionViewLayout] invalidateLayout];
    [self.collectionView scrollToItemAtIndexPath:currentIndexPath atScrollPosition:UICollectionViewScrollPositionCenteredVertically animated:NO];
}

// When the screen is rotated, update cell size to match
- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [self updateViewForCellSize:size];
}

// Calculate just once (per rotation) and re-use.
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return _cellSize;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsZero;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return cellPadding;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return cellPadding;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    return _alertHeaderSize;
}

- (CollectionHeaderView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        CollectionHeaderView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader
                                                                                    withReuseIdentifier:@"AlertHeader"
                                                                                           forIndexPath:indexPath];
        headerView.label.text = _alertHeaderText;
        return headerView;
    }
    return nil;
}

// When the user touches a photo, present a view with a larger version
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath  {
    // Only show the next view if the cell has replaced it's placeholder
    PhotoCell *cell = (PhotoCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
    if (cell.imageReceived) {
        [self performSegueWithIdentifier:@"viewPhoto" sender:indexPath];
    }
}

#pragma mark Segues

// Ensure the next view has the photo and placeholder it will need
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"viewPhoto"]) {
        NSIndexPath *indexPath = sender;
        PhotoCell *cell = (PhotoCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
        PhotoVC *photoVC = segue.destinationViewController;
        photoVC.photo = _photosDict[_photoIDSet[indexPath.row]];
        photoVC.smallPlaceholder = cell.imageView.image;
    }
}

#pragma mark CoreLocation

// This method is always called at least once (app open), and again if/when the user makes a status change.
- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    switch (status) {
        case kCLAuthorizationStatusNotDetermined:
            break;
            
        case kCLAuthorizationStatusAuthorizedWhenInUse:
            NSLog(@"User authorized CoreLocation services.");
            [self updateAppStatus];
            break;

        default:
            NSLog(@"Location services not available, status: %d.", status);
            [self updateAppStatus];
            break;
    }
}

// stopUpdatingLocation called as the query is only sent once per network status change.
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    [manager stopUpdatingLocation];
    if (!_apiSearch) {
        CLLocation *location = locations.lastObject;
        _apiSearch = [[FlickrApiSearch alloc] initWithCoordinate:location.coordinate];
    }
    [self getPhotos];
}

#pragma mark Alert message

- (void)updateAppStatusWithAlertVisible:(BOOL)visible withText:(NSString *)text {
    if (visible) {
        // Update the text and scroll to the top so the user sees the message
        _alertHeaderText = text;
        _alertHeaderSize = CGSizeMake(self.view.frame.size.width, alertHeaderHeight);
        if ([self.collectionView numberOfItemsInSection:0] > 0) {
            [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]
                                        atScrollPosition:UICollectionViewScrollPositionTop
                                                animated:YES];
        }
        
        // Image searching unavailable -- disable the RefreshControl
        [_refreshControl removeFromSuperview];
    } else {
        // Image searching available -- enable the RefreshControl
        [self.collectionView addSubview:_refreshControl];
        
        // Setting the header's size to zero will hide the header from view
        _alertHeaderSize = CGSizeZero;
    }
    
    // reloadData forces the header to be refreshed
    [self.collectionView reloadData];
}

- (void)updateAppStatus {
    if ([AFNetworkReachabilityManager sharedManager].isReachable == NO) {
        [self updateAppStatusWithAlertVisible:YES withText:@"No network available"];
        return;
    }
    
    switch ([CLLocationManager authorizationStatus]) {
        case kCLAuthorizationStatusDenied:
        case kCLAuthorizationStatusRestricted:
            [self updateAppStatusWithAlertVisible:YES withText:@"Location services disabled"];
            break;

            
        default:
            [self updateAppStatusWithAlertVisible:NO withText:nil];
            break;
    }
}

- (void)networkStatusChange:(AFNetworkReachabilityStatus)status {
    switch (status) {
        case AFNetworkReachabilityStatusNotReachable: {
            NSLog(@"Network not reachable.");
            [_locationManager stopUpdatingLocation];
            [self updateAppStatus];
            break;
        }

        case AFNetworkReachabilityStatusUnknown:
            break;
            
        default:
            NSLog(@"Network is reachable.");
            [_locationManager startUpdatingLocation];
            [self updateAppStatus];
            break;
    }
}

@end
