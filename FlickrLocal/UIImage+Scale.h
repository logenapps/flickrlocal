//
//  UIImage+Scale.h
//  FlickrLocal
//
//  Created by logen on 8/24/15.
//  Copyright (c) 2015 logenw. All rights reserved.
//

@import Foundation;
@import UIKit;

@interface UIImage (UIImage_Scale)

- (UIImage *)toScale:(CGFloat)toScale;
    
@end
