//
//  CollectionVC.h
//  FlickrLocal
//
//  Created by logen on 8/22/15.
//  Copyright (c) 2015 logenw. All rights reserved.
//

@import CoreLocation;
@import UIKit;

#import "FlickrApiSearch.h"

@interface CollectionVC : UICollectionViewController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, CLLocationManagerDelegate> {
    BOOL _activeSearch;
    BOOL _isIpad;
    CGSize _cellSize;
    CGSize _alertHeaderSize;
    UIImage *_placeholderImage;
    
}

@property (strong, nonatomic) NSString *alertHeaderText;
@property (strong, nonatomic) FlickrApiSearch *apiSearch;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) NSMutableOrderedSet *photoIDSet;
@property (strong, nonatomic) NSMutableDictionary *photosDict;
@property (strong, nonatomic) UIRefreshControl *refreshControl;

@end

