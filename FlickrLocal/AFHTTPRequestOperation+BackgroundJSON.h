//
//  AFHTTPRequestOperation+BackgroundJSON.h
//  FlickrLocal
//
//  Created by logen on 8/25/15.
//  Copyright (c) 2015 logenw. All rights reserved.
//

#import "AFNetworking.h"

@interface AFHTTPRequestOperation (AFHTTPRequestOperation_BackgroundJSON)

+ (AFHTTPRequestOperation *)operationWithURL:(NSURL *)url;

@end

