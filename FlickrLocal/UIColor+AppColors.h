//
//  UIColor+AppColors.h
//  FlickrLocal
//
//  Created by logen on 8/25/15.
//  Copyright (c) 2015 logenw. All rights reserved.
//

@import Foundation;
@import UIKit;

@interface UIColor (UIColor_AppColors)

+ (UIColor *)placeholderGray;
+ (UIColor *)systemLightBlue;

@end
