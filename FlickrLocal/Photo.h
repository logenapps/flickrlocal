//
//  Photo.h
//  FlickrLocal
//
//  Created by logen on 8/22/15.
//  Copyright (c) 2015 logenw. All rights reserved.
//

@import Foundation;
@import UIKit;

typedef NS_ENUM(NSInteger, FlickrPhotoSize) {
    FlickrPhotoSizeLargeSquare,
    FlickrPhotoSizeSmall,
    FlickrPhotoSizeMedium,
    FlickrPhotoSizeLarge
};

@interface Photo : NSObject {
    int farm;
    NSString *secret;
    NSString *server;
}

@property (nonatomic) CGSize largePhotoSize;
@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *county;
@property (nonatomic, copy) NSString *country;
@property (nonatomic, copy) NSString *latitude;
@property (nonatomic, copy) NSString *longitude;
@property (nonatomic, copy) NSString *ownerName;
@property (nonatomic, copy) NSString *photoDescription;
@property (nonatomic, copy) NSString *photoID;
@property (nonatomic, copy) NSString *region;
@property (nonatomic, copy) NSString *taken;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *views;

- (void)fetchAdditionalInfo:(void (^)(NSArray *relevantInfo))completion;
- (id)initWithMetadata:(NSDictionary *)metadata;
- (NSURL *)urlForSize:(enum FlickrPhotoSize)size;

@end
