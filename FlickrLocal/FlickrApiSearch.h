//
//  FlickrApiSearch.h
//  FlickrLocal
//
//  Created by logen on 8/22/15.
//  Copyright (c) 2015 logenw. All rights reserved.
//

@import CoreLocation;
@import Foundation;

#import "FlickrApi.h"

@interface FlickrApiSearch : FlickrApi {
    NSUInteger _page;
}

@property (nonatomic) CLLocationCoordinate2D coordinate;

- (id)initWithCoordinate:(CLLocationCoordinate2D)coordinate;
- (void)searchNearby:(void (^)(NSDictionary *photos))completion;

@end
