//
//  FlickrApiSearch.m
//  FlickrLocal
//
//  Created by logen on 8/22/15.
//  Copyright (c) 2015 logenw. All rights reserved.
//

#import "FlickrApiSearch.h"

#import "AFHTTPRequestOperation+BackgroundJSON.h"
#import "AFNetworking.h"
#import "Photo.h"

@implementation FlickrApiSearch

- (id)initWithCoordinate:(CLLocationCoordinate2D)coordinate {
    self = [super init];
    if (self) {
        _coordinate = coordinate;
        _page = 0;
    }
    return self;
}

// Eventually this could be broken up based on specific needs.
- (NSURL *)urlForSearch {
    NSString *baseCall = [NSString stringWithFormat:@"https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=%@", apiKey];
    NSString *latitude = [NSString stringWithFormat:@"&lat=%f", _coordinate.latitude];
    NSString *longitude = [NSString stringWithFormat:@"&lon=%f", _coordinate.longitude];
    NSString *perPage = @"&per_page=100";
    NSString *pageNum = [NSString stringWithFormat:@"&page=%lu", (unsigned long)_page];
    NSString *extras = @"&extras=description,url_l";
    NSString *jsonFormat = @"&format=json&nojsoncallback=1";
    NSString *urlString = [@[baseCall,
                             latitude,
                             longitude,
                             perPage,
                             pageNum,
                             extras,
                             jsonFormat]
                           componentsJoinedByString:@""];
    NSLog(@"Photos search: %@", urlString);
    return [NSURL URLWithString:urlString];
}

// Search for public photos based on provided coordinate. Results returned on background thread.
- (void)searchNearby:(void (^)(NSDictionary *photos))completion {
    // Page is incremented each time this method is called to get new results.
    _page++;
    
    
    NSURL *url = [self urlForSearch];
    AFHTTPRequestOperation *operation = [AFHTTPRequestOperation operationWithURL:url];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        // Verify the received JSON is valid according to Flickr
        NSDictionary *jsonDict = (NSDictionary *)responseObject;
        if (![self flickrCallSuccessful:jsonDict]) {
            completion(nil);
            return;
        }
        
        // Create a Photo array from the available photos metadata
        NSArray *photosMetadata = responseObject[@"photos"][@"photo"];
        NSMutableDictionary *photos = [NSMutableDictionary dictionaryWithCapacity:photosMetadata.count];
        for (NSDictionary *photoMetadata in photosMetadata) {
            Photo *photo = [[Photo alloc] initWithMetadata:photoMetadata];
            [photos setObject:photo forKey:photo.photoID];
        }
        completion([photos copy]);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@", error.localizedDescription);
        completion(nil);
    }];
    [operation start];
}

@end
