//
//  Photo.m
//  FlickrLocal
//
//  Created by logen on 8/22/15.
//  Copyright (c) 2015 logenw. All rights reserved.
//

#import "Photo.h"

#import "FlickrApiGetInfo.h"

@implementation Photo

-(id)initWithMetadata:(NSDictionary *)metadata {
    if ( self = [super init] ) {
        CGFloat largeHeight = [metadata[@"height_l"] floatValue];
        CGFloat largeWidth = [metadata[@"width_l"] floatValue];
        
        farm = [metadata[@"farm"] intValue];
        _largePhotoSize = CGSizeMake(largeWidth, largeHeight);
        _photoID = metadata[@"id"];
        _photoDescription = metadata[@"description"][@"_content"];
        secret = metadata[@"secret"];
        server = metadata[@"server"];
        _title = metadata[@"title"];
    }
    return self;
}

// All Flickr image URLs follow a similar pattern based on requested FlickrPhotoSize
- (NSURL *)urlForSize:(enum FlickrPhotoSize)size {
    NSString *apiSize;
    switch (size) {
        case FlickrPhotoSizeLargeSquare:
            apiSize = @"_q";
            break;
        case FlickrPhotoSizeSmall:
            apiSize = @"_m";
            break;
        case FlickrPhotoSizeMedium:
            apiSize = @"";
            break;
        case FlickrPhotoSizeLarge:
            apiSize = @"_b";
            break;
            
        default:
            // Will default to thumbnail size if a proper size isn't requested
            apiSize = @"t";
            break;
    }
    
    NSString *urlString = [NSString stringWithFormat:@"http://farm%d.staticflickr.com/%@/%@_%@%@.jpg", farm, server, _photoID, secret, apiSize];
    return [NSURL URLWithString:urlString];
}

- (void)fetchAdditionalInfo:(void (^)(NSArray *relevantInfo))completion {
    FlickrApiGetInfo *apiGetInfo = [[FlickrApiGetInfo alloc] init];
    [apiGetInfo getInfoForPhoto:_photoID completion:^(NSDictionary *receivedInfo) {
        if (!receivedInfo) {
            completion(nil);
            return;
        }
        
        // Update photo information, then pass back in array of dicts for use
        _city = receivedInfo[@"location"][@"locality"][@"_content"];
        _county = receivedInfo[@"location"][@"county"][@"_content"];
        _country = receivedInfo[@"location"][@"country"][@"_content"];
        _latitude = receivedInfo[@"location"][@"latitude"];
        _longitude = receivedInfo[@"location"][@"longitude"];
        _ownerName = receivedInfo[@"owner"][@"realname"];
        _region = receivedInfo[@"location"][@"region"][@"_content"];
        _taken = receivedInfo[@"dates"][@"taken"];
        _views = receivedInfo[@"views"];
        
        NSMutableArray *photosInfo = [[NSMutableArray alloc] initWithCapacity:10];
        NSString *latAndLon = [NSString stringWithFormat:@"%@, %@", _latitude, _longitude];
        
        [photosInfo addObject:[NSDictionary dictionaryWithObject:_title forKey:@"Title"]];
        [photosInfo addObject:[NSDictionary dictionaryWithObject:[self prettyLocation] forKey:@"Photo Location"]];
        [photosInfo addObject:[NSDictionary dictionaryWithObject:latAndLon forKey:@"Lat, Long"]];
        [photosInfo addObject:[NSDictionary dictionaryWithObject:_ownerName forKey:@"Photo Owner"]];
        [photosInfo addObject:[NSDictionary dictionaryWithObject:_taken forKey:@"Taken"]];
        [photosInfo addObject:[NSDictionary dictionaryWithObject:_views forKey:@"Views"]];

        completion([photosInfo copy]);
    }];
}

- (NSString *)prettyLocation {
    NSString *location = @"";
    if (_city && _region) {
        location = [NSString stringWithFormat:@"%@, %@", _city, _region];
    } else if (_county && _region) {
        location = [NSString stringWithFormat:@"%@, %@", _county, _region];
    } else if (_region && _country) {
        location = [NSString stringWithFormat:@"%@, %@", _region, _country];
    } else if (_region) {
        location = _region;
    }

    return location;
}
@end
