//
//  PhotoVC.m
//  FlickrLocal
//
//  Created by logen on 8/23/15.
//  Copyright (c) 2015 logenw. All rights reserved.
//

#import "PhotoVC.h"

#import "UIColor+AppColors.h"
#import "UIImage+Scale.h"
#import "UIImageView+AFNetworking.h"

@implementation PhotoVC

- (void)viewDidLoad {
    [super viewDidLoad];

    // Placeholder gets aspect-scaled for iPad
    _isIpad = (self.traitCollection.userInterfaceIdiom == UIUserInterfaceIdiomPad) ? YES : NO;
    if (_isIpad) {
        _scaledPlaceholder = [self enlargeImage:_smallPlaceholder];
    } else {
        _imageView.contentMode = UIViewContentModeScaleAspectFill;
        _scaledPlaceholder = _smallPlaceholder;
    }

    // Kick off Flickr API requests for larger image and more info
    [self requestLargerImage];
    [self requestAdditionalInfo];
    
    // View, scroll view configuration
    [self popoutViewVisible:NO animated:NO];
    self.automaticallyAdjustsScrollViewInsets = NO;
    _scrollView.delegate = self;
    _scrollView.minimumZoomScale = 1.0;
    _scrollView.maximumZoomScale = 6.0;

    // Update text views, informing the user if there is no title but using description as-is.
    if ([_photo.title length] == 0) {
        _titleView.text = @"<no title>";
    } else {
        _titleView.text = _photo.title;
    }
    _descriptionView.text = _photo.photoDescription;
    [_titleView scrollRangeToVisible:NSMakeRange(0, 1)];
    [_descriptionView scrollRangeToVisible:NSMakeRange(0, 1)];
}

// Stop any running image requests since the user navigated away from this view
- (void)viewDidDisappear:(BOOL)animated {
    [self.imageView cancelImageRequestOperation];
    [super viewDidDisappear:YES];
}

- (void)stopActivityIndicator {
    [self.activityView stopAnimating];
}

- (void)requestLargerImage {
    NSURLRequest *request = [NSURLRequest requestWithURL:[_photo urlForSize:FlickrPhotoSizeLarge]];
    [self.imageView setImageWithURLRequest:request
                          placeholderImage:_scaledPlaceholder
                                   success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                       // Scale the received image to ensure it fits within the maximum size
                                       self.imageView.image = [self scaleToAllowedSize:image];
                                       [self stopActivityIndicator];
                                   } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                                       NSLog(@"%@", error.localizedDescription);
                                       [self stopActivityIndicator];
                                   }];
}

- (void)requestAdditionalInfo {
    // Show an activity indicator in the navigation bar until data is available
    UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    activity.color = [UIColor systemLightBlue];
    [activity startAnimating];
    UIBarButtonItem *activityButton = [[UIBarButtonItem alloc] initWithCustomView:activity];
    self.navigationItem.rightBarButtonItem = activityButton;
    
    // Request additional data and load table view with results
    [_photo fetchAdditionalInfo:^(NSArray *newInfo) {
        // Replace the activity spinner depending on
        if (newInfo.count > 0) {
            NSLog(@"Updating photo info tableview with %lu pieces of information.", (unsigned long)newInfo.count);
            _photoInfo = newInfo;
            dispatch_async(dispatch_get_main_queue(), ^{
                [_tableView reloadData];
                UIBarButtonItem *infoButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"More Info"
                                                                                   style:UIBarButtonItemStylePlain
                                                                                  target:self action:@selector(popoutViewOpen)];
                self.navigationItem.rightBarButtonItem = infoButtonItem;
            });
        } else {
            NSLog(@"Info retrieval failed.");
            self.navigationItem.rightBarButtonItem = nil;
        }
    }];
}

#pragma mark Image methods

// On the iPad, AspectFill isn't used, so ensure the image is scaled down to maximum allowed size.
- (UIImage *)scaleToAllowedSize:(UIImage *)image {
    if (_isIpad) {
        CGFloat maxLength = fmaxf(image.size.width, image.size.height);
        CGFloat widthConstraint = _imageViewWidthConstraint.constant;
        if (maxLength > widthConstraint) {
            CGFloat imageScale = widthConstraint / maxLength;
            return [image toScale:imageScale];
        }
    }

    // Return unmodified image if iPhone or image is already small enough.
    return image;
}


// When scaling the placeholder, consider device displayScale as well as the view's square constraints.
- (UIImage *)enlargeImage:(UIImage *)image {
    CGFloat displayScale = self.view.traitCollection.displayScale;
    CGFloat largePhotoMaxLength = fmaxf(_photo.largePhotoSize.width, _photo.largePhotoSize.height);
    CGFloat widthConstraint = _imageViewWidthConstraint.constant;
    CGFloat constrainedFutureLength = fminf(largePhotoMaxLength / displayScale, widthConstraint);
    CGFloat placeholderPhotoMaxLength = fmaxf(image.size.height, image.size.width);
    CGFloat imageScale = constrainedFutureLength / placeholderPhotoMaxLength;

    return [image toScale:imageScale];
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return self.imageView;
}

#pragma mark Popout Tableview

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _photoInfo.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"InfoCell" forIndexPath:indexPath];
    NSDictionary *dict = _photoInfo[indexPath.row];
    cell.textLabel.text = [[dict allKeys] objectAtIndex:0];
    cell.detailTextLabel.text = [dict objectForKey:cell.textLabel.text];
    
    return cell;
}

// Setting height to 0 is ineffective, but 0.1 works fine.
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.1f;
}

// The popoutView's animation in and out is controlled via the left and right autolayout constraints.
- (void)popoutViewVisible:(BOOL)visible animated:(BOOL)animated {
    if (_isIpad) {
        _popoutViewRightConstraint.constant = (visible) ? 0.0f : -_popoutViewWidthConstraint.constant;
    } else {
        CGFloat maxWidth = fmaxf(self.view.frame.size.width, self.view.frame.size.height);
        _popoutViewLeftConstraint.constant = (visible) ? 0.0f : maxWidth;
        _popoutViewRightConstraint.constant = (visible) ? 0.0f : -maxWidth;
    }
    
    if (animated) {
        [UIView animateWithDuration:0.25f
                         animations:^ {
                             [self.view layoutIfNeeded];
                         }];
    } else {
        [self.view layoutIfNeeded];
    }
}

- (void)popoutViewOpen {
    [self popoutViewVisible:YES animated:YES];
}

- (IBAction)popoutViewClose:(id)sender {
    [self popoutViewVisible:NO animated:YES];
}

@end
