//
//  PhotoCell.h
//  FlickrLocal
//
//  Created by logen on 8/22/15.
//  Copyright (c) 2015 logenw. All rights reserved.
//

@import Foundation;
@import UIKit;

@interface PhotoCell : UICollectionViewCell

@property (nonatomic) BOOL imageReceived;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;

@end
