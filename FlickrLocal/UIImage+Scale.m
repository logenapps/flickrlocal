//
//  UIImage+Scale.m
//  FlickrLocal
//
//  Created by logen on 8/24/15.
//  Copyright (c) 2015 logenw. All rights reserved.
//

#import "UIImage+Scale.h"

@implementation UIImage (UIImage_Scale)

- (UIImage *)toScale:(CGFloat)toScale {
    CGSize size = CGSizeApplyAffineTransform(self.size, CGAffineTransformMakeScale(toScale, toScale));
    UIGraphicsBeginImageContextWithOptions(size, NO, 0.0);
    [self drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return scaledImage;
}

@end
