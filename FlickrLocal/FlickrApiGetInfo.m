//
//  FlickrApiGetInfo.m
//  FlickrLocal
//
//  Created by logen on 8/25/15.
//  Copyright (c) 2015 logenw. All rights reserved.
//

#import "FlickrApiGetInfo.h"

#import "AFHTTPRequestOperation+BackgroundJSON.h"
#import "AFNetworking.h"

@implementation FlickrApiGetInfo

- (NSURL *)urlForPhotoInfo:(NSString *)photoID {
    NSString *baseCall = [NSString stringWithFormat:@"https://api.flickr.com/services/rest/?method=flickr.photos.getInfo&api_key=%@", apiKey];
    NSString *photoString = [NSString stringWithFormat:@"&photo_id=%@", photoID];
    NSString *jsonFormat = @"&format=json&nojsoncallback=1";
    NSString *urlString = [@[baseCall,
                             photoString,
                             jsonFormat]
                           componentsJoinedByString:@""];
    NSLog(@"Photo getInfo: %@", urlString);
    return [NSURL URLWithString:urlString];
}

- (void)getInfoForPhoto:(NSString *)photoID completion:(void (^)(NSDictionary *info))completion {
    NSURL *url = [self urlForPhotoInfo:photoID];
    AFHTTPRequestOperation *operation = [AFHTTPRequestOperation operationWithURL:url];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        // Verify the received JSON is valid according to Flickr
        NSDictionary *jsonDict = (NSDictionary *) responseObject;
        if (![self flickrCallSuccessful:jsonDict]) {
            completion(nil);
            return;
        }
        
        // Create a dictionary holding certain relevant items from the response
        completion(jsonDict[@"photo"]);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@", error.localizedDescription);
        completion(nil);
    }];
    [operation start];
}

@end
