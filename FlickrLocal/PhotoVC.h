//
//  PhotoVC.h
//  FlickrLocal
//
//  Created by logen on 8/23/15.
//  Copyright (c) 2015 logenw. All rights reserved.
//

@import UIKit;

#import "Photo.h"

@interface PhotoVC : UIViewController <UIScrollViewDelegate, UITableViewDataSource, UITableViewDelegate> {
    BOOL _isIpad;
    NSArray *_photoInfo;
    UIImage *_scaledPlaceholder;
}

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageViewWidthConstraint;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityView;
@property (weak, nonatomic) IBOutlet UITextView *descriptionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *popoutViewLeftConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *popoutViewRightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *popoutViewWidthConstraint;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITextView *titleView;
@property (strong, nonatomic) Photo *photo;
@property (strong, nonatomic) UIImage *smallPlaceholder;

- (IBAction)popoutViewClose:(id)sender;

@end
