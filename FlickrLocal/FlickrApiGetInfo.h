//
//  FlickrApiGetInfo.h
//  FlickrLocal
//
//  Created by logen on 8/25/15.
//  Copyright (c) 2015 logenw. All rights reserved.
//

#import "FlickrApi.h"

@interface FlickrApiGetInfo : FlickrApi

- (void)getInfoForPhoto:(NSString *)photoID completion:(void (^)(NSDictionary *info))completion;

@end
