//
//  CollectionHeaderView.h
//  FlickrLocal
//
//  Created by logen on 8/25/15.
//  Copyright (c) 2015 logenw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionHeaderView : UICollectionReusableView

@property (strong, nonatomic) IBOutlet UILabel *label;

@end
