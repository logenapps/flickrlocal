//
//  FlickrApi.m
//  FlickrLocal
//
//  Created by logen on 8/25/15.
//  Copyright (c) 2015 logenw. All rights reserved.
//

#import "FlickrApi.h"

@implementation FlickrApi

// Flickr returns "stat": "fail" in unsuccessful API calls
- (BOOL)flickrCallSuccessful:(NSDictionary *)jsonDict {
    if ([@"fail" isEqualToString:jsonDict[@"stat"]]) {
        NSLog(@"API call failure, code %@: %@", jsonDict[@"code"], jsonDict[@"message"]);
        return NO;
    }
    
    return YES;
}

@end
