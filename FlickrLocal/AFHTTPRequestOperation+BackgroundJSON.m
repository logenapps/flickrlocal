//
//  AFHTTPRequestOperation+BackgroundJSON.m
//  FlickrLocal
//
//  Created by logen on 8/25/15.
//  Copyright (c) 2015 logenw. All rights reserved.
//

#import "AFHTTPRequestOperation+BackgroundJSON.h"

@implementation AFHTTPRequestOperation (AFHTTPRequestOperation_BackgroundJSON)

// Convenience method for operation with JSON serialization, background queue.
+ (AFHTTPRequestOperation *)operationWithURL:(NSURL *)url {
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    operation.completionQueue = dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0);
    
    return operation;
}

@end
