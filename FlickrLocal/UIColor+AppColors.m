//
//  UIColor+AppColors.m
//  FlickrLocal
//
//  Created by logen on 8/25/15.
//  Copyright (c) 2015 logenw. All rights reserved.
//

#import "UIColor+AppColors.h"

@implementation UIColor (UIColor_AppColors)

+ (UIColor *)placeholderGray {
    return [UIColor colorWithRed:220.0f/255.0f green:220.0f/255.0f blue:220.0f/255.0f alpha:1.0f];
}

+ (UIColor *)systemLightBlue {
    return [UIColor colorWithRed:0.0f green:122.0f/255.0f blue:255.0f/255.0f alpha:1.0f];
}

@end
