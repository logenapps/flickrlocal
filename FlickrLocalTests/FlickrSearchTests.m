//
//  FlickrSearchTests.m
//  FlickrSearchTests
//
//  Created by logen on 8/26/15.
//  Copyright (c) 2015 logenw. All rights reserved.
//

@import CoreLocation;
@import UIKit;
@import XCTest;

#import "FlickrApiSearch.h"

@interface FlickrApiSearch (FlickrApiSearchTesting)

- (NSURL *)urlForSearch;

@end

@interface FlickrSearchTests : XCTestCase

@property (nonatomic) CLLocationCoordinate2D coordinate;
@property (strong, nonatomic) FlickrApiSearch *apiSearch;

@end

@implementation FlickrSearchTests

- (void)setUp {
    [super setUp];
    
    _coordinate = CLLocationCoordinate2DMake(37.332407, -122.030469);
    _apiSearch = [[FlickrApiSearch alloc] initWithCoordinate:_coordinate];
}

- (void)tearDown {
    [super tearDown];
}

- (void)testUrlWithIncrementingPage {
    NSURL *url1 = [_apiSearch urlForSearch];
    NSURL *correct1 = [NSURL URLWithString:@"https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=549855cebc62373a02ada2a14738ee83&lat=37.332407&lon=-122.030469&per_page=100&page=0&extras=description,url_l&format=json&nojsoncallback=1"];
    XCTAssertEqualObjects(url1, correct1, @"URL comparison 1 failed");
    
    // Search once, should increment
    [_apiSearch searchNearby:^(NSDictionary *test){}];
    NSURL *correct2 = [NSURL URLWithString:@"https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=549855cebc62373a02ada2a14738ee83&lat=37.332407&lon=-122.030469&per_page=100&page=1&extras=description,url_l&format=json&nojsoncallback=1"];
    NSURL *url2 = [_apiSearch urlForSearch];
    XCTAssertEqualObjects(url2, correct2, @"URL comparison 2 failed");
    
    // Call twice, should increment twice
    [_apiSearch searchNearby:^(NSDictionary *test){}];
    [_apiSearch searchNearby:^(NSDictionary *test){}];
    NSURL *correct3 = [NSURL URLWithString:@"https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=549855cebc62373a02ada2a14738ee83&lat=37.332407&lon=-122.030469&per_page=100&page=3&extras=description,url_l&format=json&nojsoncallback=1"];
    NSURL *url3 = [_apiSearch urlForSearch];
    XCTAssertEqualObjects(url3, correct3, @"URL comparison 3 failed");
}

@end