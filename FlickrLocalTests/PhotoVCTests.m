//
//  PhotoVCTests.m
//  FlickrLocal
//
//  Created by logen on 8/26/15.
//  Copyright (c) 2015 logenw. All rights reserved.
//

@import Foundation;
@import UIKit;
@import XCTest;

#import "UIImage+Color.h"
#import "PhotoVC.h"

@interface PhotoVC (PhotoVCTests)

- (UIImage *)scaleToAllowedSize:(UIImage *)image;
- (UIImage *)enlargeImage:(UIImage *)image;

@end

@interface PhotoVCTests : XCTestCase

@property (strong, nonatomic) UIImage *onePxImage;
@property (strong, nonatomic) PhotoVC *photoVC;
@property (strong, nonatomic) NSArray *photoInfo;

@end

@implementation PhotoVCTests

- (void)setUp {
    [super setUp];
    
    _onePxImage = [UIImage imageWithColor:[UIColor blueColor]];
    _photoVC = [[PhotoVC alloc] init];
    _photoVC.photo = [[Photo alloc] init];
    _photoVC.photo.largePhotoSize = CGSizeMake(1024.0, 768.0);
    _photoVC.photo.photoID = @"20696926091";
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testScaledToAllowedSize {
    UIImage *scaledImage = [_photoVC scaleToAllowedSize:_onePxImage];
    CGFloat width = scaledImage.size.width;
    CGFloat height = scaledImage.size.height;
    if (_photoVC.traitCollection.userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        XCTAssertEqual(width, 500.0, "width should equal 500.0 (max)");
        XCTAssertEqual(height, 500.0, "height should equal 500.0 (max)");
    } else {
        XCTAssertEqual(width, 1.0, "width should equal 1.0 (unchanged)");
        XCTAssertEqual(height, 1.0, "height should equal 1.0 (unchanged)");
    }

}

- (void)testFetchAdditionalInfo {
    _photoVC.photo.title = @"My test image";
    XCTAssertEqual(_photoInfo.count, 0, "datasource should start empty");
    XCTestExpectation *expectation = [self expectationWithDescription:@"Testing info fetch worked!"];
    [_photoVC.photo fetchAdditionalInfo:^(NSArray *info) {
        
        XCTAssertEqual(info.count, 6, "datasource should have 6 fields");
        [expectation fulfill];
    }];
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            XCTFail(@"Expectation failed with error: %@", error);
        }
    }];
}

@end
