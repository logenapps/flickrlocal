//
//  ImageScaleTests.m
//  FlickrLocal
//
//  Created by logen on 8/26/15.
//  Copyright (c) 2015 logenw. All rights reserved.
//

@import UIKit;
@import XCTest;

#import "UIImage+Scale.h"
#import "UIImage+Color.h"

@interface ImageScaleTests : XCTestCase

@property (strong, nonatomic) UIImage *onePxImage;

@end

@implementation ImageScaleTests

- (void)setUp {
    [super setUp];
    _onePxImage = [UIImage imageWithColor:[UIColor blueColor]];
}

- (void)tearDown {
    [super tearDown];
}

- (void)testScaleUp {
    CGFloat test1 = 100.0;
    UIImage *testImage1 = [_onePxImage toScale:test1];
    CGFloat height = testImage1.size.height;
    XCTAssertEqual(height, test1, @"height not equal");
    CGFloat width = testImage1.size.width;
    XCTAssertEqual(width, test1, @"width not equal");
    
    CGFloat test2 = 533.0;
    UIImage *testImage2 = [_onePxImage toScale:test2];
    height = testImage2.size.height;
    XCTAssertEqual(height, test2, @"height not equal");
    width = testImage2.size.width;
    XCTAssertEqual(width, test2, @"width not equal");
    
}

- (void)testScaleDown {
    CGFloat startSize = 100.0;
    UIImage *testImage1 = [_onePxImage toScale:startSize];
    
    // test scaling back down
    CGFloat scale = 0.60f;
    UIImage *testImage2 = [testImage1 toScale:scale];
    CGFloat height = testImage2.size.height;
    CGFloat width = testImage2.size.width;
    XCTAssertGreaterThan(height, 60, @"height too short");
    XCTAssertLessThan(height, 61, @"height too tall");
    XCTAssertGreaterThan(width, 60, @"width too short");
    XCTAssertLessThan(width, 61, @"width too tall");
}

@end
