# Flickr Local #

### Features ###

* Public images from Flickr are fetched based on user's location (~5km radius) into a "gallery view" of thumbnails.
* 100 images are retrieved at a time, with "pull to refresh" used to keep finding more local photos.
* Dedicated photo view that adjusts based on size class, with support for "pinch to zoom" on images and additional information.
* Universal design designed for iPads and iPhones running iOS 8.4.

### Also of note ###

* AutoLayout and SizeClasses are used throughout the app.
    * This app supports iPads and iPhones in the 3 standard orientations (Portrait, Landscape Left/Right).
* Thumbnails are used in gallery view to increase performance.
    * iPads show more thumbnails at one time.
    * Thumbnails are dynamically resized based on device and orientation, to ensure edge-to-edge appearance of thumbnails.
* Tapping a thumbnail loads the dedicated photo view, which behaves differently based on device type:
    * iPhones will show the image fullscreen, with portions of the image cropped if necessary.
    * iPads will retain the image's aspect ratio, scaling the image so that they are at most 500px by 500px.
    * iPads will display the image's full title and description below the image (if available).
    * If the thumbnail is not available, users are not able to access dedicated photo view. 
* While in the dedicated photo view, all devices will load additional photo information accessible through the "More Info" button at top-right.
    * All devices animate this "pop-out" view open and closed.
    * iPhones will show this view over the entire image.
    * iPads will show this view full-screen height but with a limited width, for better appearance.
* The app will prevent multiple pages of images to be requested from Flickr at one time.
* If it is detected that the user has lost their network connection:
    * An alert will be displayed in the gallery header.
    * The user's view will be scrolled to the top automatically so that they can't miss this information.
    * Pull to refresh is disabled.
* If the user chooses not to authorize the app to use Location Services:
    * An alert will be displayed in the gallery header.
    * No images will be retrieved from Flickr.
    * Pull to refresh is disabled.
* If the user's network returns, or if they authorize Location Services later (in device Settings):
    * Any relevant alert is removed.
    * Images begin getting fetched automatically.
    * Pull to refresh is re-enabled.


### Installation ###
* This project was created using Xcode 6.4 and targets iOS 8.4.
* This project leverages [CocoaPods](https://cocoapods.org/) and [AFNetworking](https://github.com/AFNetworking/AFNetworking).
* Steps
    1. Close Xcode 6.4
    2. Clone this project
    3. Install CocoaPods if necessary
    4. Navigate to the project directory, then run: `pod install`
    5. Always open the workspace in order to build the project: "FlickrLocal.xcworkspace"